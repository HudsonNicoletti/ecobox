# Phalcon Skeleton
  Put together by Hudson Nicoletti.

## Whats this for?

This is a Skeleton for a simple, powerfull MVC Application that uses the Phalcon framework.

## Requirements

* PHP >= 7.4
* Phalcon >= 4.0.0
* Composer

### DEPENDENCIES
```shell
$ cd libraries
$ composer install
```
### REQUIREMENTS
  - Composer
  - LAMP
    - Apache
    - Mysql
    - PHP 7.4
  - [PhalconPHP](https://phalconphp.com/en/download)
  - Apache AllowOverride (mod_rewrite)

### DATABASE MIGRATIONS
Uses Migrations from Phalcon's Devtools

`MIGRATE`
```shell
$ phalcon migration --action=run --migrations=.phalcon/migrations --data=always
```

`GENERATE`
```shell
$ phalcon migration --action=generate --migrations=.phalcon/migrations --data=always
```
