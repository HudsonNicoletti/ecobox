$("[data-ajaxForm='true']").submit(function(){

    var $this   = $(this),
        action  = $this.attr("action"),
        method  = $this.attr("method")
        inputs  = $this.find("input:not(:file):not(:submit) , textarea"),
        files   = $this.find("input[type=file]"),
        content = new FormData(  );

        //  Loop & append inputs
        for( var i = 0;  i < inputs.length ; ++i )
        {
            content.append( $(inputs[i]).attr("name") , $(inputs[i]).val() ); // Add all fields automatic
        }

        if( files.length  ) {
            for( var i = 0;  i < files.length ; ++i )
            {
                if(files[i].files[i] != undefined)
                {
                    content.append(files.eq(i).attr("name"), files[i].files[i], files[i].files[i].name );// add files if exits
                }
            }
        }

        //  Submit data
        $.ajax({
            url:  action,           //  Action  ( PHP SCRIPT )
            type: method,           //  Method
            data: content,          //  Data Created
            processData: false,     //  Tell jQuery not to process the data
            contentType: false,     //  Tell jQuery not to set contentType
            dataType: "json",       //  Accept JSON response
            cache: false,           //  Disale Cashing
            success: function( response )
            {
              var _text = $("#response h1");
              _text.removeClass("animate__animated animate__bounceOut");
              _text.addClass("animate__animated animate__bounceIn");
              _text.text(response.response);

              setTimeout(function(){
                _text.removeClass("animate__animated animate__bounceIn");
                _text.addClass("animate__animated animate__bounceOut");
              }, 5000);

              if(response.redirect)
              {
                setTimeout(function(){
                window.location.href = response.redirect;
                }, 5000);
              }
            }
        });

        event.preventDefault();
        return false;
});

$("#ckeditor").summernote({
  height: 150,
  toolbar: [
          ['font', ['bold', 'underline', 'clear']],
          ['insert', ['link','picture']]
        ],
  callbacks: {
    onChange: function(contents, $editable) {
      $("#ckeditor").val(contents);
    }
  }
});
