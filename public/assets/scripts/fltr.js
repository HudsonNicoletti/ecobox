/*!
Filtr.js - http://hudsonncioletti.github.io/jQuery-Filtr-Plugin/
Copyright (c) 2014 Hudson Nicoletti
*/
!function(e){e.fn.filtr=function(t){var r=this,i=e.extend({target:null,targetChild:null,caseSensitive:!1,invert:!1},t);if(null==i.targetChild)var n=e(i.target);else var n=e(i.target).find(i.targetChild);r.keyup(function(){var t=this.value.split();return""==this.value?void n.show():void(i.invert===!0?(n.show(),n.filter(function(){for(var n=e(this),a=0;a<t.length;++a)if(n.is(":contains('"+t[a]+"')"))return!0;return!1}).hide()):(n.hide(),n.filter(function(){for(var n=e(this),a=0;a<t.length;++a)if(n.is(":contains('"+t[a]+"')"))return!0;return!1}).show()))}),i.caseSensitive===!1&&(jQuery.expr[":"].contains=function(e,t,r){return jQuery(e).text().toUpperCase().indexOf(r[3].toUpperCase())>=0})}}(jQuery);
