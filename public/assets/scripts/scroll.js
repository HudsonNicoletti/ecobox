//use window.scrollY
var scrollpos = window.scrollY;
var header = document.getElementById("nav");

function add_class_on_scroll() {
    header.classList.add("active");
}

function remove_class_on_scroll() {
    header.classList.remove("active");
}

window.addEventListener('scroll', function(){
    scrollpos = window.scrollY;

    if(scrollpos > 60){
        add_class_on_scroll();
    }
    else {
        remove_class_on_scroll();
    }

    var tabs = ($(".tabs").offset().top - 90 );
    var contact = $(".contact").offset().top;

    if(scrollpos > tabs && scrollpos < (tabs + 190))
    {
        document.getElementById("index").classList.remove("active")
        document.getElementById("about").classList.add("active")
    }
    else if(scrollpos > (tabs + 190))
    {
      document.getElementById("index").classList.add("active")
      document.getElementById("about").classList.remove("active")
    }
    else if(scrollpos < (tabs + 190))
    {
      document.getElementById("index").classList.add("active")
      document.getElementById("about").classList.remove("active")
    }


    if(scrollpos > (contact - 60))
    {
        document.getElementById("index").classList.remove("active")
        document.getElementById("contact").classList.add("active")
    }
    else
    {
      document.getElementById("index").classList.add("active")
      document.getElementById("contact").classList.remove("active")
    }
});
