$( document ).ready(function() {
  $("#nature").change();
});
$("#nature").on("change", function(data){

  $("[data-ocupacao]").hide();

  switch ($("#nature").val()) {

    case "aposentado-pensionista":
      $("[data-ocupacao='1']").show();
    break;
    case "assalariado":
      $("[data-ocupacao='2']").show();
    break;
    case "liberal":
      $("[data-ocupacao='2']").show();
    break;
    case "autonomo":
      $("[data-ocupacao='2']").show();
    break;
    case "funcionario-publico":
      $("[data-ocupacao='2']").show();
    break;
    case "empresario":
      $("[data-ocupacao='3']").show();
    break;
    default:

  }

});

$("[data-addr]").change();

$("[data-addr]").on("change", function(data){

  switch ($(this).val())
  {
    case "same":
      $("[data-other]").hide();
    break;
    case "other":
      $("[data-other]").attr("style","display: flex;");
    break;


  }

});


$('[data-cpf]').formatter({
  'pattern': '{{999}}-{{999}}-{{999}}-{{99}}'
});

$('[data-cnpj]').formatter({
  'pattern': '{{99}}.{{999}}.{{999}}/{{9999}}-{{99}}'
});

$('[data-phone]').formatter({
  'pattern': '({{999}}){{99999}}-{{9999}}'
});

$('[data-date]').formatter({
  'pattern': '{{99}}/{{99}}/{{9999}}'
});

$('[data-rg]').formatter({
  'pattern': '{{99}}.{{999}}.{{999}}-{{9}}'
});

$('[data-money]').formatter({
  'pattern': 'R$ {{99999999}}'
});

$('[data-yrs]').formatter({
  'pattern': '{{999}} Anos'
});

$('[data-number]').formatter({
  'pattern': '{{999999999999}}'
});
