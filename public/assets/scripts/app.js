
$(".pane.icons > [data-pane]").on("click", function(i){
  let data = $(this).attr("data-pane");


  $(".pane.icons > [data-pane].active").removeClass("active");
  $(this).addClass("active");

  $(".pane.descriptions > [data-pane]").removeClass("active");
  $(".pane.descriptions > [data-pane='"+data+"']").addClass("active");

});

$("ul.titles > [data-tab]").on("click", function(i){
  let data = $(this).attr("data-tab");


  $("ul.titles > [data-tab].active").removeClass("active");

  $(this).addClass("active");

  $("ul.contents > [data-tab]").removeClass("active");
  $("ul.contents > [data-tab='"+data+"']").addClass("active");
});

$(".clients > [data-client]").on("click", function(i){
  let data = $(this).attr("data-client");


  $(".clients > [data-client].active").removeClass("active");
  $(this).addClass("active");

  $(".clients .texts > [data-client]").removeClass("active");
  $(".clients .texts > [data-client='"+data+"']").addClass("active");

});

$('#about').on("click",function(){
  var offset = ($(".tabs").offset().top - 90 );

  scroll(0, offset);
})

$('#contact').on("click",function(){
  var offset = ($(".contact").offset().top - 90 );

  scroll(0, offset);
})
$('#learn').on("click",function(){
  var offset = ($("#install").offset().top - 90 );

  scroll(0, offset);
})
