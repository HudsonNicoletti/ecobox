<?php

use Phalcon\Mvc\Application;

try {

    #   Include Composer
    require __DIR__ . '/../libraries/autoload.php';

    #   Include services
    require __DIR__ . '/../config/services.php';

    #   Handle the request
    $application = new Application($di);

    #   Include modules
    require __DIR__ . '/../config/modules.php';

    echo $application->handle($_SERVER['REQUEST_URI'])->getContent();

}
catch (Exception $e) {

    # show debug message if in debug mode else will redirect to error page to user
    if( $cf->server->debug )
    {
        echo $e->getMessage();
    }
    else
    {
        header("Location: /error/ServerError");
    }

}
