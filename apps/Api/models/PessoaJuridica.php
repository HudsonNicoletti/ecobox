<?php

namespace Api\Models;

class PessoaJuridica extends \Phalcon\Mvc\Model
{
  public function initialize()
  {
      $this->setSource("pj");
  }
}
