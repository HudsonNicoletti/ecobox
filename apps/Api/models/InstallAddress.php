<?php

namespace Api\Models;

class InstallAddress extends \Phalcon\Mvc\Model
{
  public function initialize()
  {
      $this->setSource("install_address");
  }
}
