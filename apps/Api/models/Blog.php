<?php

namespace Api\Models;

class Blog extends \Phalcon\Mvc\Model
{
  public function initialize()
  {
      $this->setSource("blog_posts");
  }
}
