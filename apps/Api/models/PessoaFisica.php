<?php

namespace Api\Models;

class PessoaFisica extends \Phalcon\Mvc\Model
{
  public function initialize()
  {
      $this->setSource("pf");
  }
}
