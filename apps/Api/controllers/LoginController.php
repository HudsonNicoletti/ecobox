<?php
namespace Api\Controllers;

use Api\Models\Users    as Users;


class LoginController extends ControllerBase
{
  /*
  *  Handles login request ,checks if the request is valid
  *
  *  @throws InvalidRequestMethod if the provided method is not post
  *  @throws AccessDenied if the user does not exist
  *  @throws AccessDenied if the password is invalid
  *  @throws InvalidCsrfToken if the csrf is invalid|expired
  *  @throws Unknown if the user information could not be loaded
  *
  *  @return JSON Returns flags if wass a success or an error for Ajax
  */
  public function loginAction()
  {
    $this->response->setContentType("application/json");  # set response type of json for ajax                  # use custom functions

    $username   = preg_replace('/\s+/', '', $this->request->getPost("username","string"));  # Get username input & filter as a string
    $password   = preg_replace('/\s+/', '', $this->request->getPost("password","string"));  # Get password input & filter as a string
    $user       = ( !$this->isEmail( $username ) ) ? Users::findFirstByUsername($username) : Users::findFirstByEmail($username); # check if username input is a valid email or regular username

    try
    {
      if(!$this->request->isPost()):                            # Accept only POST request
        throw new \Exception("Invalid Request") ;                # throws exception if not POST request

      elseif(!$user->_):                                        # checks if user does not exist
        throw new \Exception("Usuário Inválido") ;                # throws exception if user does not exist

      elseif(!password_verify( $password , $user->password )):  # checks if password input is invalid
        throw new \Exception("Senha Inválida") ;                  # throws exception if password is invalid

      elseif(!$this->security->checkToken()):                   # checks if CSRF token is invalid
        throw new \Exception("Invalid Token") ;                  # throws exception if CSRF token is invalid

      else:

        $this->session->set("ecobox_session", $user->_);   # start a session with user id

        $this->flags= [                                # returns flags for ajax
          'response' =>  "Bem-Vindo, {$user->name} !" ,
          'redirect' => "/admin"        # set wellcome message to user
        ];

      endif;

    }
    catch(\Exception $e)
    {
      $this->flags = [            # returns flags for ajax
        'response' => $e->getMessage(),    # sets exception message
      ];
    }

    return $this->response->setJsonContent($this->flags);   # set json content to flags

    $this->response->send();                                # send content to view
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);   # set view with no templating , only plain action view for JSON
  }

  /*
  * Handles logout request, terminates current session and redirects to home page
  *
  * @return Phalcon\Http\Response
  */
  public function LogoutAction()
  {
    $this->session->destroy();                      # terminate session
    return $this->response->redirect('/admin',true);     # redirect to index
  }

}
