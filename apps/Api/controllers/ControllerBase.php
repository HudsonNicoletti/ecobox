<?php

namespace Api\Controllers;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
  protected $flags = [];

  public function initialize()
  {

  }

  protected function isEmail($e)
  {
    return preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $e );
  }

  protected function uniqueCode($prefix = null, $limit = 10)
  {
    $limit = ($prefix ? $limit - strlen($prefix) : $limit);

    return $prefix.str_shuffle(substr(md5(round(time().uniqid())), 0, $limit));
  }

  protected function makeSlug($string){
    return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
  }
}
