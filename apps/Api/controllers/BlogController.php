<?php
namespace Api\Controllers;

use Api\Models\Blog    as Blog;

class BlogController extends ControllerBase
{

  public function newPostAction()
  {
    $this->response->setContentType("application/json");  # set response type of json for ajax                  # use custom functions

    $title   = $this->request->getPost("title","string");
    $text   = $this->request->getPost("text");

    try
    {
      if(!$this->request->isPost()):                            # Accept only POST request
        throw new \Exception("Invalid Request") ;                # throws exception if not POST request

      elseif(!$title != ""):
        throw new \Exception("Título nao deve ser vazio") ;
      elseif(!$text != ""):
        throw new \Exception("Texto nao deve ser vazio") ;
      elseif(!$this->request->hasFiles()):
        throw new \Exception("Selecione uma Imagem") ;                # throws exception if user does not exist

      elseif(!$this->security->checkToken()):                   # checks if CSRF token is invalid
        throw new \Exception("Invalid Token") ;                  # throws exception if CSRF token is invalid

      else:

        foreach($this->request->getUploadedFiles() as $file):
          $filename = substr(sha1(uniqid()), 0, 12).'.'.$file->getExtension();
          $file->moveTo("assets/images/blog/{$filename}");
        endforeach;

        $post = new Blog;
          $post->title    = $title;
          $post->slug     = $this->makeSlug($title);
          $post->text     = $text;
          $post->image = $filename;
          $post->date  = (new \DateTime())->format("Y-m-d H:i:s");
        if(!$post->save())
        {
          throw new \Exception("Tente Novamente.") ;
        }


        $this->flags = [                                # returns flags for ajax
          'response' =>  "Públicado com sucesso!" ,
          'redirect' => "/admin/blog"        # set wellcome message to user
        ];
      endif;

    }
    catch(\Exception $e)
    {
      $this->flags = [            # returns flags for ajax
        'response' => $e->getMessage(),
        'redirect' => "/admin/blog/new"
      ];
    }

    return $this->response->setJsonContent($this->flags);   # set json content to flags

    $this->response->send();                                # send content to view
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);   # set view with no templating , only plain action view for JSON
  }

  public function updatePostAction()
  {
    $this->response->setContentType("application/json");  # set response type of json for ajax                  # use custom functions

    $title   = $this->request->getPost("title","string");
    $text   = $this->request->getPost("text");
    $slug   = $this->request->getPost("slug");

    try
    {
      if(!$this->request->isPost()):                            # Accept only POST request
        throw new \Exception("Invalid Request") ;                # throws exception if not POST request

      elseif(!$title != ""):
        throw new \Exception("Título nao deve ser vazio") ;
      elseif(!$text != ""):
        throw new \Exception("Texto nao deve ser vazio") ;

      elseif(!$this->security->checkToken()):                   # checks if CSRF token is invalid
        throw new \Exception("Invalid Token") ;                  # throws exception if CSRF token is invalid

      else:

        $post = Blog::findFirstBySlug($slug);

        if($this->request->hasFiles()) {

        unlink("assets/images/blog/{$post->image}");

        foreach($this->request->getUploadedFiles() as $file):
          $filename = substr(sha1(uniqid()), 0, 12).'.'.$file->getExtension();
          $file->moveTo("assets/images/blog/{$filename}");
        endforeach;

          $post->image = $filename;
        }

          $post->title    = $title;
          $post->slug     = ($this->makeSlug($title) != $slug) ? $this->makeSlug($title) : $slug;
          $post->text     = $text;
        if(!$post->save())
        {
          throw new \Exception("Tente Novamente.") ;
        }


        $this->flags = [                                # returns flags for ajax
          'response' =>  "Alterado com sucesso!" ,
          'redirect' => "/admin/blog"        # set wellcome message to user
        ];
      endif;

    }
    catch(\Exception $e)
    {
      $this->flags = [            # returns flags for ajax
        'response' => $e->getMessage(),
        'redirect' => "/admin/blog/new"
      ];
    }

    return $this->response->setJsonContent($this->flags);   # set json content to flags

    $this->response->send();                                # send content to view
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);   # set view with no templating , only plain action view for JSON
  }

  public function deletePostAction()
  {
    $this->response->setContentType("application/json");  # set response type of json for ajax                  # use custom functions

    $slug   = $this->request->getPost("slug");

    try
    {
      if(!$this->request->isPost()):                            # Accept only POST request
        throw new \Exception("Invalid Request") ;                # throws exception if not POST request

      elseif(!$slug != ""):
        throw new \Exception("Tente Novamente") ;

      elseif(!$this->security->checkToken()):                   # checks if CSRF token is invalid
        throw new \Exception("Invalid Token") ;                  # throws exception if CSRF token is invalid

      else:

        $post = Blog::findFirstBySlug($slug);

        unlink("assets/images/blog/{$post->image}");

        if(!$post->delete())
        {
          throw new \Exception("Tente Novamente.") ;
        }


        $this->flags = [                                # returns flags for ajax
          'response' =>  "Removido com sucesso!" ,
          'redirect' => "/admin/blog"        # set wellcome message to user
        ];
      endif;

    }
    catch(\Exception $e)
    {
      $this->flags = [            # returns flags for ajax
        'response' => $e->getMessage(),
        'redirect' => "/admin/blog/new"
      ];
    }

    return $this->response->setJsonContent($this->flags);   # set json content to flags

    $this->response->send();                                # send content to view
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);   # set view with no templating , only plain action view for JSON
  }

}
