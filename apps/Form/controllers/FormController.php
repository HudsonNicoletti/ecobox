<?php

namespace Form\Controllers;

use Api\Models\PessoaFisica as PessoaFisica,
    Api\Models\PessoaJuridica as PessoaJuridica,
    Api\Models\Rep as Rep,
    Api\Models\Professional as Professional,
    Api\Models\Address as Address,
    Api\Models\InstallAddress as InstallAddress;


use Mustache_Engine as Mustache;

use Phalcon\Mvc\View;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Hidden;

use \DateTime as DateTime;

class FormController extends ControllerBase
{

  public function IndexAction()
  {
    $this->cookies->get('current_type')->delete();

    if ($this->cookies->has('unique_id')) {

      $a = PessoaFisica::findFirstByUniqueId($this->cookies->get('unique_id'));
      ($a != null) ?? $a->delete();
      $a = PessoaJuridica::findFirstByUniqueId($this->cookies->get('unique_id'));
      ($a != null) ?? $a->delete();
      $a = Professional::findFirstByUniqueId($this->cookies->get('unique_id'));
      ($a != null) ?? $a->delete();
      $a = Rep::findFirstByUniqueId($this->cookies->get('unique_id'));
      ($a != null) ?? $a->delete();
      $a = Address::findFirstByUniqueId($this->cookies->get('unique_id'));
      ($a != null) ?? $a->delete();
      $a = InstallAddress::findFirstByUniqueId($this->cookies->get('unique_id'));
      ($a != null) ?? $a->delete();
      $this->cookies->get('unique_id')->delete();
    }

    $this->view->pick("index/index");
  }


  public function beforeDadosAction()
  {
    $unique = $this->uniqueCode();
    $this->cookies->set('current_type',$this->request->getPost("type","string"));

    switch ($this->cookies->get('current_type'))
    {
      case 'pf':
        $p = new PessoaFisica;
        $p->unique_id = $unique;

      break;

      case 'pj':
        $p = new PessoaJuridica;
        $p->unique_id = $unique;
      break;
    }

    if ($p->save()) {
      $this->cookies->set('unique_id',$unique);

      $this->dispatcher->forward([
        "controller" => "form",
        "action"     => "dados"
      ]);
    }
  }


  public function DadosAction()
  {

    $this->view->selection =  $this->_form->options;
    $this->view->states = $this->_form->states;

    switch ($this->cookies->get('current_type')) {
      case 'pf':
        $this->view->pick("pf/index");
      break;

      case 'pj':
        $this->view->pick("pj/index");
      break;
    }

  }

  public function beforeRepresentanteAction()
  {
    switch ($this->cookies->get('current_type'))
    {
      case 'pf':
        $current = PessoaFisica::findFirstByUniqueId($this->cookies->get('unique_id'));
        $current->name      = $this->request->getPost('name',"string");
        $current->mother    = $this->request->getPost('mother',"string");
        $current->birthdate = (DateTime::createFromFormat('d/m/Y',$this->request->getPost('birth',"string")))->format("Y-m-d H:i:s");
        $current->email     = $this->request->getPost('email',"string");
        $current->phone     = $this->request->getPost('phone',"string");
        $current->cpf       = $this->request->getPost('cpf',"string");
        $current->value     = $this->request->getPost('value',"string");
        $current->rg        = $this->request->getPost('rg',"string");
        $current->sex       = $this->request->getPost('sex',"string");
        $current->nationality = $this->request->getPost('nationality',"string");
        $current->status    = $this->request->getPost('status',"string");

      break;

      case 'pj':
        $current = PessoaJuridica::findFirstByUniqueId($this->cookies->get('unique_id'));
        $current->cnpj        = $this->request->getPost('cnpj',"string");
        $current->social      = $this->request->getPost('social',"string");
        $current->fantasy     = $this->request->getPost('fantasy',"string");
        $current->income      = $this->request->getPost('income',"string");
        $current->capitol     = $this->request->getPost('capitol',"string");
        $current->phone       = $this->request->getPost('phone',"string");
        $current->time        = $this->request->getPost('time',"string");
        $current->area        = $this->request->getPost('area',"string");
        $current->cep         = $this->request->getPost('cep',"string");
        $current->address     = $this->request->getPost('address',"string");
        $current->number      = $this->request->getPost('number',"string");
        $current->neighbourhood      = $this->request->getPost('neighbourhood',"string");
        $current->state      = $this->request->getPost('state',"string");
        $current->city       = $this->request->getPost('city',"string");
      break;
    }

    if ($current->save()) {
      $this->dispatcher->forward([
        "controller" => "form",
        "action"     => "representante"
      ]);
    }

  }

  public function representanteAction()
  {
    $options_slug = [];
    $occupacion_slug = [];

    foreach($this->_form->options->natureza as $options)
    {
      $slug = $this->makeSlug($options);
      array_push($options_slug, $slug);
    }

    foreach($this->_form->options->ocupacao as $options)
    {
      $slug = $this->makeSlug($options);
      array_push($occupacion_slug, $slug);
    }

    $this->view->selection       =  $this->_form->options;
    $this->view->selection_slugs =  $options_slug;
    $this->view->occupacion_slug =  $occupacion_slug;
    $this->view->states = $this->_form->states;


    switch ($this->cookies->get('current_type')) {
      case 'pf':
        $this->view->info = PessoaFisica::findFirstByUniqueId($this->cookies->get('unique_id'));
        $this->view->pick("pf/representante");
      break;

      case 'pj':
        $this->view->pick("pj/representante");
      break;
    }

  }

  public function beforeEnderecoAction()
  {

        $current = new Rep;
        $current->unique_id = $this->cookies->get('unique_id');
        $current->name      = $this->request->getPost('name',"string");
        $current->mother    = $this->request->getPost('mother',"string");
        $current->birthdate = (DateTime::createFromFormat('d/m/Y',$this->request->getPost('birth',"string")))->format("Y-m-d H:i:s");
        $current->email     = $this->request->getPost('email',"string");
        $current->phone     = $this->request->getPost('phone',"string");
        $current->cpf       = $this->request->getPost('cpf',"string");
        $current->value     = $this->request->getPost('value',"string");
        $current->rg        = $this->request->getPost('rg',"string");
        $current->sex       = $this->request->getPost('sex',"string");
        $current->nationality = $this->request->getPost('nationality',"string");
        $current->status    = $this->request->getPost('status',"string");

        $pro = new Professional;
        $pro->unique_id = $this->cookies->get('unique_id');

        switch ($this->request->getPost('nature',"string")) {
          case 'aposentado-pensionista':
            $pro->nature    = $this->request->getPost('nature',"string");
            $pro->income    = $this->request->getPost('income',"string");
            $pro->time      = $this->request->getPost('retired',"string");
            $pro->benefits  = $this->request->getPost('benefits',"string");
          break;
          case 'assalariado':
            $pro->nature    = $this->request->getPost('nature',"string");
            $pro->occupation = $this->request->getPost('occupation',"string");
            $pro->income    = $this->request->getPost('salary',"string");
            $pro->time      = $this->request->getPost('job',"string");
            $pro->benefits  = $this->request->getPost('benefits',"string");
          break;
          case 'liberal':
            $pro->nature    = $this->request->getPost('nature',"string");
            $pro->occupation = $this->request->getPost('occupation',"string");
            $pro->income    = $this->request->getPost('salary',"string");
            $pro->time      = $this->request->getPost('job',"string");
            $pro->benefits  = $this->request->getPost('benefits',"string");
          break;
          case 'autonomo':
            $pro->nature    = $this->request->getPost('nature',"string");
            $pro->occupation = $this->request->getPost('occupation',"string");
            $pro->income    = $this->request->getPost('salary',"string");
            $pro->time      = $this->request->getPost('job',"string");
            $pro->benefits  = $this->request->getPost('benefits',"string");
          break;
          case 'funcionario-publico':
            $pro->nature    = $this->request->getPost('nature',"string");
            $pro->occupation = $this->request->getPost('occupation',"string");
            $pro->income    = $this->request->getPost('salary',"string");
            $pro->time      = $this->request->getPost('job',"string");
            $pro->benefits  = $this->request->getPost('benefits',"string");
          break;
          case 'empresario':
            $pro->cnpj        = $this->request->getPost('cnpj',"string");
            $pro->social      = $this->request->getPost('social',"string");
            $pro->fantasy     = $this->request->getPost('fantasy',"string");
            $pro->income      = $this->request->getPost('faturamento',"string");
            $pro->capitol     = $this->request->getPost('capitol',"string");
            $pro->phone       = $this->request->getPost('phone',"string");
            $pro->cep         = $this->request->getPost('cep',"string");
            $pro->address     = $this->request->getPost('address',"string");
            $pro->number      = $this->request->getPost('number',"string");
            $pro->neighbourhood      = $this->request->getPost('neighbourhood',"string");
            $pro->state      = $this->request->getPost('state',"string");
            $pro->city       = $this->request->getPost('city',"string");
          break;
        }

    if ($current->save() && $pro->save()) {
      $this->dispatcher->forward([
        "controller" => "form",
        "action"     => "endereco"
      ]);
    }

  }

  public function enderecoAction()
  {

    $this->view->states = $this->_form->states;


    switch ($this->cookies->get('current_type')) {
      case 'pf':
        $this->view->pick("pf/endereco");
      break;

      case 'pj':
        $this->view->pick("pj/endereco");
      break;
    }

  }

  public function beforeFinalAction()
  {
    $addr = new Address;
    $install = new InstallAddress;

    $install->unique_id = $this->cookies->get('unique_id');

    $addr->unique_id = $this->cookies->get('unique_id');
    $addr->cep = $this->request->getPost('cep',"string");
    $addr->address = $this->request->getPost('address',"string");
    $addr->number = $this->request->getPost('address_num',"string");
    $addr->neighbourhood = $this->request->getPost('neighbourhood',"string");
    $addr->state = $this->request->getPost('state',"string");
    $addr->city = $this->request->getPost('city',"string");

    switch ($this->request->getPost('useaddr',"string")) {
      case 'same':
        $install->cep = $this->request->getPost('cep',"string");
        $install->address = $this->request->getPost('address',"string");
        $install->number = $this->request->getPost('address_num',"string");
        $install->neighbourhood = $this->request->getPost('neighbourhood',"string");
        $install->state = $this->request->getPost('state',"string");
        $install->city = $this->request->getPost('city',"string");
        $install->save();
      break;
      case 'other':
        $install->cep = $this->request->getPost('install_cep',"string");
        $install->address = $this->request->getPost('install_address',"string");
        $install->number = $this->request->getPost('install_address_num',"string");
        $install->neighbourhood = $this->request->getPost('install_neighbourhood',"string");
        $install->state = $this->request->getPost('install_state',"string");
        $install->city = $this->request->getPost('install_city',"string");
        $install->save();
      break;
    }

    if ($addr->save() && $install->save()) {
      $this->dispatcher->forward([
        "controller" => "form",
        "action"     => "final"
      ]);
    }

  }

  public function finalAction()
  {
    $unique = $this->cookies->get('unique_id');

    $ct = file_get_contents($this->request->getServer('SERVER_NAME')."/admin/forms/view/{$unique}");

    $this->mail->functions->From       = $this->configuration->mail->email;
    $this->mail->functions->FromName   = "EcoBox Site";

    $this->mail->functions->addAddress($this->configuration->mail->email,  $this->configuration->mail->name);
    $this->mail->functions->Subject = "Formulario Cadastrado No Site";

    $this->mail->functions->Body = $ct;

    $this->mail->functions->send();
    $this->mail->functions->ClearAddresses();

    $unique->delete();
    $this->view->pick("index/finalizar");

  }

  public function deleteFormAction()
  {
    $this->response->setContentType("application/json");  # set response type of json for ajax                  # use custom functions

    $slug   = $this->request->getPost("slug");

    try
    {
      if(!$this->request->isPost()):                            # Accept only POST request
        throw new \Exception("Invalid Request") ;                # throws exception if not POST request

      elseif(!$slug != ""):
        throw new \Exception("Tente Novamente") ;

      elseif(!$this->security->checkToken()):                   # checks if CSRF token is invalid
        throw new \Exception("Invalid Token") ;                  # throws exception if CSRF token is invalid

      else:

        $p = PessoaFisica::findFirstByUniqueId($slug);
        $r = Rep::findFirstByUniqueId($slug);
        $p = Professional::findFirstByUniqueId($slug);
        $a = Address::findFirstByUniqueId($slug);
        $i = InstallAddress::findFirstByUniqueId($slug);

        if( $p )
        {
          $p->delete();
        }
        else
        {
          $p = PessoaJuridica::findFirstByUniqueId($slug);
          $p->delete();
        }

        $r->delete();
        $p->delete();
        $a->delete();
        $i->delete();




        $this->flags = [                                # returns flags for ajax
          'response' =>  "Removido com sucesso!" ,
          'redirect' => "/admin/forms"        # set wellcome message to user
        ];
      endif;

    }
    catch(\Exception $e)
    {
      $this->flags = [            # returns flags for ajax
        'response' => $e->getMessage(),
        'redirect' => "/admin/forms"
      ];
    }

    return $this->response->setJsonContent($this->flags);   # set json content to flags

    $this->response->send();                                # send content to view
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);   # set view with no templating , only plain action view for JSON

  }


}
