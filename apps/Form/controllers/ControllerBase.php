<?php

namespace Form\Controllers;

use Phalcon\Mvc\Controller;

# call all controllers available from api namespace
use Form\Controllers as Form;

class ControllerBase extends Controller
{

  public function initialize()
  {
    $this->assets
    ->addCss('/assets/stylesheets/form.css')
    ->addJs('/assets/scripts/jquery.min.js')
    ->addJs('/assets/scripts/jquery.formatter.min.js')
    ->addJs('/assets/scripts/form.js');

  }

  protected function isEmail($str)
  {
    return preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $str );
  }

  protected function makeSlug($string){
    return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
  }

  protected function uniqueCode($prefix = null, $limit = 10)
  {
    $limit = ($prefix ? $limit - strlen($prefix) : $limit);

    return $prefix.str_shuffle(substr(md5(round(time().uniqid())), 0, $limit));
  }

}
