<?php

namespace Form;

use Phalcon\Loader,
    Phalcon\Mvc\View,
    Phalcon\Config\Adapter\Ini,
    Phalcon\Mvc\ModuleDefinitionInterface,
    Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

class Module
{
  public function registerAutoloaders()
  {
    $loader = new Loader;

    $loader->registerNamespaces([
      'Form\Controllers' => __DIR__ . '/controllers/',
      'Api\Models' => __DIR__ . '/../Api/models/',
    ]);

    $loader->register();
  }

  public function registerServices($di)
  {

    $di['view'] = function() {
      $view = new View;
      $view->setViewsDir(__DIR__ . '/views/');

      return $view;
    };

  }
}
