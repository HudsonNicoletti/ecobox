<?php

namespace Website\Controllers;


use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Hidden;

use Phalcon\Mvc\View;

class IndexController extends ControllerBase
{
  public $photos = [];

  public function IndexAction()
  {
    $form = new Form();

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken()
      ]);

      $element['name'] = new Text( "name" ,[
        'required' => true,
      ]);
      $element['last'] = new Text( "last" ,[
        'required' => true,
      ]);
      $element['email'] = new Text( "email" ,[
        'required' => true,
      ]);
      $element['phone'] = new Text( "phone" ,[
        'required' => false,
      ]);
      $element['msg'] = new Textarea( "msg" ,[
        'required' => false,
      ]);

      foreach ($element as $e)
      {
        $form->add($e);
      }

    $this->view->form = $form;
    $this->view->pick("index/index");
  }

  public function instagramAction(){
    $this->response->setContentType("application/json");

    $instagram = $this->instagramImages();

    return $this->response->setJsonContent([
      "response" =>  $this->photos
    ]);
    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function instagramImages()
  {
    $medias = $this->insta->getMediasByTag('ecoboxengenharia',8);

    for ($i=0; $i < count($medias) ; $i++) {

      $content = file_get_contents($medias[$i]->getImageHighResolutionUrl());
      $base64 = strval('data:image/jpg;base64,' . base64_encode($content));
      // base64 encoding since image will not appear in html with link directly
      array_push($this->photos, [ $base64, $medias[$i]->getLink()]);
    }
  }

}
