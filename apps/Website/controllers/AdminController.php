<?php

namespace Website\Controllers;

use Api\Models\Blog    as Blog,
    Api\Models\InstallAddress as InstallAddress,
    Api\Models\Address as Address,
    Api\Models\Rep as Rep,
    Api\Models\Professional as Professional,
    Api\Models\PessoaFisica as PessoaFisica,
    Api\Models\PessoaJuridica as PessoaJuridica;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Hidden;

use Mustache_Engine as Mustache;
use Phalcon\Mvc\View;

class AdminController extends ControllerBase
{
  public function onConstruct()
  {
    if( !$this->session->has("ecobox_session") && $this->router->getControllerName() == 'admin' && $this->router->getActionName() != "index")
    {
      $this->response->redirect("/admin/logout");
    }

  }

  public function IndexAction()
  {
    $this->assets
    ->addCss('/assets/stylesheets/admin.css')
    ->addJs('/assets/scripts/admin.js');

    $form = new Form();

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken()
    ]);

    $element['username'] = new Text( "username" ,[
      'required' => true,
      'name' => 'username'
    ]);

    $element['password'] = new Password( "password" ,[
      'required' => true,
      'name' => 'password'
    ]);

    foreach ($element as $e)
    {
      $form->add($e);
    }

    $this->view->form = $form;
    $this->view->pick("admin/index");
  }

  public function DashboardAction()
  {
    $this->assets
    ->addCss('/assets/stylesheets/admin.css')
    ->addJs('/assets/scripts/admin.js');
    $this->view->pick("admin/dashboard");
  }

  public function BlogAction()
  {
    $posts = Blog::find([
      "order" => "date DESC"
    ]);
    $this->view->posts = $posts;

    $this->assets
    ->addCss('/assets/stylesheets/admin.css')
    ->addJs('/assets/scripts/admin.js');
    $this->view->pick("admin/blog");
  }

  public function newPostAction()
  {
    $this->assets
    ->addCss('/assets/stylesheets/admin.css')
    ->addCss('https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css')
    ->addCss('https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css')
    ->addJs('https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js')
    ->addJs('https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js')
    ->addJs('/assets/scripts/admin.js');

    $form = new Form();

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken()
    ]);

    $element['title'] = new Text( "title" ,[
      'required' => true,
      'name' => 'title'
    ]);

    $element['file'] = new File( "file" ,[
      'required' => true,
      'name' => 'file'
    ]);

    $element['text'] = new Textarea( "text" ,[
      'required' => true,
      'name' => 'text',
      'id' => 'ckeditor'
    ]);

    foreach ($element as $e)
    {
      $form->add($e);
    }

    $this->view->form = $form;
    $this->view->pick("admin/new");
  }

  public function editPostAction()
  {
    $this->assets
    ->addCss('/assets/stylesheets/admin.css')
    ->addCss('https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css')
    ->addCss('https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css')
    ->addJs('https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js')
    ->addJs('https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js')
    ->addJs('/assets/scripts/admin.js');

    $post = Blog::findFirstBySlug($this->dispatcher->getParam("slug"));

    $form = new Form();

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken()
    ]);

    $element['slug'] = new Hidden( "slug" ,[
      'name'  => "slug",
      'value' => $post->slug
    ]);

    $element['title'] = new Text( "title" ,[
      'required' => true,
      'name' => 'title',
      'value' => $post->title
    ]);

    $element['file'] = new File( "file" ,[
      'name' => 'file'
    ]);

    $element['text'] = new Textarea( "text" ,[
      'required' => true,
      'name' => 'text',
      'id' => 'ckeditor',
      'value' => $post->text
    ]);

    foreach ($element as $e)
    {
      $form->add($e);
    }

    $this->view->form = $form;
    $this->view->pick("admin/edit");
  }

  public function removePostAction()
  {
    $this->assets
    ->addCss('/assets/stylesheets/admin.css')
    ->addJs('/assets/scripts/admin.js');

    $post = Blog::findFirstBySlug($this->dispatcher->getParam("slug"));

    $form = new Form();

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken()
    ]);

    $element['slug'] = new Hidden( "slug" ,[
      'name'  => "slug",
      'value' => $post->slug
    ]);

    foreach ($element as $e)
    {
      $form->add($e);
    }

    $this->view->form = $form;
    $this->view->pick("admin/remove");
  }

  public function FormsAction()
  {
    $this->assets
    ->addCss('/assets/stylesheets/admin.css')
    ->addJs('/assets/scripts/admin.js');
    $this->view->pick("admin/forms");

    $forms = InstallAddress::query()
    ->columns([
      'Api\Models\InstallAddress.unique_id',
      'Api\Models\PessoaFisica.name',
      'Api\Models\PessoaFisica.cpf',
      'Api\Models\PessoaJuridica.social',
      'Api\Models\PessoaJuridica.cnpj'
    ])
    ->leftJoin('Api\Models\PessoaFisica', 'Api\Models\InstallAddress.unique_id = Api\Models\PessoaFisica.unique_id')
    ->leftJoin('Api\Models\PessoaJuridica', 'Api\Models\InstallAddress.unique_id = Api\Models\PessoaJuridica.unique_id')

    ->execute();

    $this->view->forms = $forms;

  }

  public function removeformAction()
  {
    $this->assets
    ->addCss('/assets/stylesheets/admin.css')
    ->addJs('/assets/scripts/admin.js');

    $form = new Form();

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken()
    ]);

    $element['slug'] = new Hidden( "slug" ,[
      'name'  => "slug",
      'value' => $this->dispatcher->getParam("slug")
    ]);

    foreach ($element as $e)
    {
      $form->add($e);
    }

    $this->view->form = $form;
    $this->view->pick("admin/removeform");
  }

  public function viewFormAction()
  {
    $unique = $this->dispatcher->getParam("slug");

    $data = PessoaFisica::findFirstByUniqueId($unique);
    $pj = PessoaJuridica::findFirstByUniqueId($unique);
    $rep = Rep::findFirstByUniqueId($unique);
    $pro = Professional::findFirstByUniqueId($unique);
    $addr = Address::findFirstByUniqueId($unique);
    $install = InstallAddress::findFirstByUniqueId($unique);

    $parse = [
      'logo'    => 'data: '.mime_content_type("assets/images/logo/Branca.png").';base64,'.base64_encode(file_get_contents("assets/images/logo/Branca.png")),
      'rep_name'    => $rep->name,
      'rep_mother'  => $rep->mother,
      'rep_birth'   => $rep->birthdate,
      'rep_email'   => $rep->email,
      'rep_phone'   => $rep->phone,
      'rep_cpf'     => $rep->cpf,
      'rep_rg'      => $rep->rg,
      'rep_value'   => $rep->value,
      'rep_sex'     => $rep->sex,
      'rep_nationality'   => $rep->nationality,
      'rep_status'    => $rep->status,
      'nature'        => $pro->nature,
      'occupation'    => $pro->occupation,
      'income'        => $pro->income,
      'time'          => $pro->time,
      'address'       => $addr->address,
      'number'        => $addr->number,
      'cep'           => $addr->cep,
      'neighbourhood' => $addr->neighbourhood,
      'state'         => $addr->state,
      'city'          => $addr->city,
      'install_address'  => $install->address,
      'install_number'   => $install->number,
      'install_cep'   => $install->cep,
      'install_neighbourhood'   => $install->neighbourhood,
      'install_state'   => $install->state,
      'install_city'    => $install->city
    ];

    switch ($pro->nature)
    {
      case 'aposentado-pensionista':
          $parse["aposentado"] = true;
          $parse["benefits"]   = $pro->benefits;
      break;
      case 'empresario':
          $parse["empresario"] = true;
          $parse["cnpj"]   = $pro->cnpj;
          $parse["social"]   = $pro->social;
          $parse["fantasy"]   = $pro->fantasy;
          $parse["capitol"]   = $pro->capitol;
          $parse["phone"]     = $pro->phone;
          $parse["em_cep"]    = $pro->cep;
          $parse["em_address"]   = $pro->address;
          $parse["em_number"]   = $pro->number;
          $parse["em_neighbourhood"]   = $pro->neighbourhood;
          $parse["em_state"]   = $pro->state;
      break;
      default:
        $parse["normal"] = true;
      break;
    }

    if (@$data->unique_id)
    {
        $parse['pf']      = "true";
        $parse['name']    = $data->name;
        $parse['mother']  = $data->mother;
        $parse['birth']   = $data->birthdate;
        $parse['email']   = $data->email;
        $parse['phone']   = $data->phone;
        $parse['cpf']     = $data->cpf;
        $parse['rg']      = $data->rg;
        $parse['value']   = $data->value;
        $parse['sex']     = $data->sex;
        $parse['nationality'] = $data->nationality;
        $parse['status']      = $data->status;
    }
    else
    {
        $parse['pj']      = true;
        $parse["pj_cnpj"]   = $pj->cnpj;
        $parse["pj_social"]   = $pj->social;
        $parse["pj_fantasy"]   = $pj->fantasy;
        $parse["pj_capitol"]   = $pj->capitol;
        $parse["pj_phone"]     = $pj->phone;
        $parse["pj_cep"]    = $pj->cep;
        $parse["pj_address"]   = $pj->address;
        $parse["pj_number"]   = $pj->number;
        $parse["pj_neighbourhood"]   = $pj->neighbourhood;
        $parse["pj_state"]   = $pj->state;
        $parse["pj_city"]   = $pj->city;
        $parse["pj_income"]   = $pj->income;
        $parse["pj_time"]   = $pj->time;
        $parse["pj_area"]   = $pj->area;
    }

    echo (new Mustache)->render(file_get_contents(__DIR__."/../views/admin/formview.phtml"), $parse);

    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
