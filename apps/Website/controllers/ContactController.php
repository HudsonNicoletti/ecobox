<?php

namespace Website\Controllers;

use Mustache_Engine as Mustache;

class ContactController extends ControllerBase
{
  public function IndexAction()
  {
    # contact in indexview
  }


  public function EmailAction()
  {
    $this->response->setContentType("application/json");
    $response = "";
    $name  = $this->request->getPost("name");
    $last  = $this->request->getPost("last");
    $email = $this->request->getPost("email");
    $phone = $this->request->getPost("phone");
    $msg   = $this->request->getPost("msg");

      try
      {
        if(!$this->request->isPost()):
          throw new \Exception("Request Method Is Invalid");

        elseif(!$email || !$this->isEmail($email)):
          throw new \Exception("E-Mail Preenchido Invalido");

        elseif(!$this->security->checkToken()):
          throw new \Exception("Invalid CSRF Token");
        endif;
      }
      catch (\Exception $e)
      {
        $response = $e->getMessage();
      }

      # send email
      $this->mail->functions->From       = $email;
      $this->mail->functions->FromName   = $name;

      $this->mail->functions->addAddress($this->configuration->mail->email,  $this->configuration->mail->name);
      $this->mail->functions->Subject = "{$name} {$last}, Entrou em contato pelo site!";

      $this->mail->functions->Body = (new Mustache)->render(file_get_contents(__DIR__."/../views/contact_email.phtml"), [
        'logo'    => 'data: '.mime_content_type("assets/images/logo/Branca.png").';base64,'.base64_encode(file_get_contents("assets/images/logo/Branca.png")),
        'name'    => $name,
        'last'    => $last,
        'email'   => $email,
        'message' => $msg
      ]);

      if(!$this->mail->functions->send())
      {
        $response = "Não foi possível enviar, tente novamente";
      }
      else{
        $response = "Enviado com Sucesso!";
      }
      $this->mail->functions->ClearAddresses();

      return $this->response->setJsonContent([
        "response" =>  $response
      ]);

      $this->response->send();
      $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

  }
}
