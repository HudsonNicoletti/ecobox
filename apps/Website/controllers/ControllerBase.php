<?php

namespace Website\Controllers;

use Phalcon\Mvc\Controller;

# call all controllers available from api namespace
use Api\Controllers as API;

class ControllerBase extends Controller
{

  public function initialize()
  {
    $this->assets
    ->addCss('/assets/stylesheets/animate.css')
    ->addCss('/assets/stylesheets/responsive.css')
    ->addCss('/assets/stylesheets/main.css')
    ->addCss('https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css')
    ->addJs('/assets/scripts/jquery.min.js')
    ->addJs("https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js",false)
    ->addJs("/assets/scripts/jquery.paroller.min.js",false)
    ->addJs('/assets/scripts/scroll.js')
    ->addJs('/assets/scripts/app.js');

  }

  protected function isEmail($str)
  {
    return preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $str );
  }


}
