<?php

namespace Website\Controllers;

use Api\Models\Blog    as Blog;

class BlogController extends ControllerBase
{
  public $formatter;

  public function onConstruct()
  {
    $this->formatter = new \IntlDateFormatter('pt_BR', \IntlDateFormatter::LONG, \IntlDateFormatter::LONG);

    $this->formatter->setPattern("d 'de' MMMM y");
  }

  public function IndexAction()
  {
    $posts = Blog::find([
      "order" => "date DESC"
    ]);

    $this->assets->addJs("/assets/scripts/fltr.js");

    $this->view->formatter = $this->formatter;
    $this->view->posts = $posts;
    $this->view->pick("blog/index");
  }

  public function blogpostAction()
  {
    $url_slug = $this->dispatcher->getParam("slug","string");
    $posts = Blog::findFirstBySlug($url_slug);

    $this->view->formatter = $this->formatter;
    $this->view->post = $posts;
    $this->view->pick("blog/post");

  }
}
