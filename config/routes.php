<?php

$router->add("/contato/enviar", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'contact',
  'action'     => 'email'
])->via(["POST"]);

$router->add("/instagram", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'index',
  'action'     => 'instagram'
])->via(["GET"]);

$router->add("/blog", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'blog',
  'action'     => 'index'
]);

$router->add("/blog/{slug:[a-zA-Z0-9\_\-]+}", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'blog',
  'action'     => 'blogpost'
]);

$router->add("/instagram/{slug:[a-zA-Z0-9\_\-]+}", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'index',
  'action'     => 'InstagramViewImage'
]);

$router->add("/admin", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => ($active_session) ? "dashboard" : "index"
]);

$router->add("/admin/login", [
  "namespace"  => "Api\Controllers",
  "module"     => "Api",
  'controller' => 'login',
  'action'     => 'login'
]);

$router->add("/admin/logout", [
  "namespace"  => "Api\Controllers",
  "module"     => "Api",
  'controller' => 'login',
  'action'     => 'logout'
]);

$router->add("/admin/blog", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => "blog"
]);


$router->add("/admin/blog/new", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => "newPost"
]);

$router->add("/admin/blog/create", [
  "namespace"  => "Api\Controllers",
  "module"     => "Api",
  'controller' => 'blog',
  'action'     => "newPost"
])->via(["POST"]);


$router->add("/admin/blog/edit/{slug:[a-zA-Z0-9\_\-]+}", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => 'editpost'
]);


$router->add("/admin/blog/update", [
  "namespace"  => "Api\Controllers",
  "module"     => "Api",
  'controller' => 'blog',
  'action'     => "updatePost"
])->via(["POST"]);


$router->add("/admin/blog/remove/{slug:[a-zA-Z0-9\_\-]+}", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => 'removepost'
]);


$router->add("/admin/blog/delete", [
  "namespace"  => "Api\Controllers",
  "module"     => "Api",
  'controller' => 'blog',
  'action'     => "deletePost"
])->via(["POST"]);


$router->add("/admin/forms", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => "forms"
]);


$router->add("/formulario", [
  "namespace"  => "Form\Controllers",
  "module"     => "Form",
  'controller' => 'form',
  'action'     => 'index'
]);

$router->add("/formulario/dados", [
  "namespace"  => "Form\Controllers",
  "module"     => "Form",
  'controller' => 'form',
  'action'     => "beforeDados"
])->via(["POST"]);

$router->add("/formulario/representante", [
  "namespace"  => "Form\Controllers",
  "module"     => "Form",
  'controller' => 'form',
  'action'     => "beforerepresentante"
])->via(["POST"]);

$router->add("/formulario/endereco", [
  "namespace"  => "Form\Controllers",
  "module"     => "Form",
  'controller' => 'form',
  'action'     => "beforeendereco"
])->via(["POST"]);

$router->add("/formulario/finalizar", [
  "namespace"  => "Form\Controllers",
  "module"     => "Form",
  'controller' => 'form',
  'action'     => "beforefinal"
])->via(["POST"]);

$router->add("/email", [
  "namespace"  => "Form\Controllers",
  "module"     => "Form",
  'controller' => 'form',
  'action'     => 'emailview'
]);

$router->add("/admin/forms/view/{slug:[a-zA-Z0-9\_\-]+}", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => 'viewForm'
]);

$router->add("/admin/forms/remove/{slug:[a-zA-Z0-9\_\-]+}", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => 'removeform'
]);


$router->add("/admin/forms/delete", [
  "namespace"  => "Form\Controllers",
  "module"     => "Form",
  'controller' => 'form',
  'action'     => "deleteForm"
])->via(["POST"]);
