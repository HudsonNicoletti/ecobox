<?php
/**
  * Here you register all the modules for your application if you ever need more/less.
  *
  *
  *
**/

$application->registerModules([
  'Api' => [
    'className' => 'Api\Module',
    'path'      => __DIR__ . '/../apps/Api/Module.php'
  ],
  'Form' => [
    'className' => 'Form\Module',
    'path'      => __DIR__ . '/../apps/Form/Module.php'
  ],
  'Website' => [
    'className' => 'Website\Module',
    'path'      => __DIR__ . '/../apps/Website/Module.php'
  ]
]);
