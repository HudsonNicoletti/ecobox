<?php

/**
  * Here is declared all Phalcon Needs to function
**/

use Phalcon\Mvc\Dispatcher\Exception as DispatchException,
    Phalcon\Session\Adapter\Stream as SessionAdapter,
    Phalcon\Session\Manager as SessionManager,
    Phalcon\Db,
    Phalcon\Db\Exception,
    Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter,
    Phalcon\Mvc\Dispatcher as PhDispatcher,
    Phalcon\Events\Manager as EventsManager,
    Phalcon\Url as UrlResolver,
    Phalcon\Config\Adapter\Ini,
    Phalcon\DI\FactoryDefault,
    Phalcon\Mvc\Router,
    Phalcon\Dispatcher;

use PHPMailer\PHPMailer\PHPMailer;
use Phpfastcache\Helper\Psr16Adapter;

/**
  * Generates a new Default && grabs the config.json data
**/
$di = new FactoryDefault();
$cf = (Object)json_decode(file_get_contents( __DIR__ . "/../config/config.json"));
$_f = json_decode(file_get_contents( __DIR__ . "/../config/form_options.json"));
$_s = json_decode(file_get_contents( __DIR__ . "/../config/states.json"));

/**
  * Here we initiate the database configuration
**/
$di['db'] = function() use ($cf) {

    $connection = new DbAdapter([
      "host"      => $cf->database->host,
      "username"  => $cf->database->username,
      "password"  => $cf->database->password,
      "dbname"    => $cf->database->dbname
    ]);

   return $connection;
};

#    The URL component is used to generate all kinds of URLs in the application
$di['_form'] = function () use ($_f,$_s) {

    return (object)["options"=>$_f,"states"=>$_s];
};

#    The URL component is used to generate all kinds of URLs in the application
$di['url'] = function () {
    $url = new UrlResolver();
    $url->setBaseUri('/');

    return $url;
};

#   Starts the session the first time some component requests the session service
$di['session'] = function (){
  $session = new SessionManager();
  $files = new SessionAdapter([
      'savePath' => sys_get_temp_dir(),
  ]);
  $session->setAdapter($files);
  $session->start();

  return $session;
};

# Cookies & crypt need eachother to function.
$di['cookies'] = function() {
    $cookies = new Phalcon\Http\Response\Cookies();
    $cookies->useEncryption(true);
    return $cookies;
};

$di['crypt'] = function() use ($cf) {
    $crypt = new Phalcon\Crypt();
    $crypt->setKey( md5($cf->database->host) );
    return $crypt;
};

/**
  * Here is where i cana access any config inside a controller via $this->configuration
**/
$di['configuration'] = function () use ($cf) {
    return (object)[
      "database"      => $cf->database ,
      "mail"          => $cf->mail,
      "debug"         => $cf->server->debug,
    ];
};


#   Handles 404
$di['dispatcher'] = function () {
    $eventsManager = new EventsManager();
    $eventsManager->attach("dispatch:beforeException", function($event, $dispatcher, $exception) {

      if ($exception instanceof DispatchException) {
        $dispatcher->forward(array(
          'controller' => 'Error',
          'action'     => 'NotFound'
        ));
        return false;
      }

    });

    $dispatcher = new PhDispatcher();
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
};

#   Configure PHPMailer ( loaded by composer ) , returning the mail ini config & PHPMailer functions
$di['mail'] = function () use ($cf) {
    $mail = new PHPMailer;

    $mail->isSMTP();
    $mail->isHTML(true);

    $mail->CharSet      = $cf->mail->charset;
    $mail->Host         = $cf->mail->host;
    $mail->SMTPAuth     = true;
    $mail->Username     = $cf->mail->username;
    $mail->Password     = $cf->mail->password;
    $mail->SMTPSecure   = $cf->mail->security;
    $mail->Port         = $cf->mail->port;

    #   Pass as object only MAIL config and PHPMailer Functions.
    return (object)[
      "name" => $cf->mail->name ,
      "email" => $cf->mail->email ,
      "functions" => $mail
    ];
};

$di['insta'] = function () use ($cf) {
  $instagram = \InstagramScraper\Instagram::withCredentials(new \GuzzleHttp\Client(), $cf->GuzzleHttp[0], $cf->GuzzleHttp[1], new Psr16Adapter('Files'));
  $instagram->login();
  $instagram->saveSession();
  return $instagram;
};

#    Registering a router
$di['router'] = function () use ($di){
    $router = new Router();

    # check if login session is active
    $session = $di['session'];
    $active_session = $session->has("ecobox_session");

    $router->setDefaultModule('Website');
    $router->setDefaultNamespace('Website\Controllers');
    $router->removeExtraSlashes(true);

    require( __DIR__ . "/routes.php" );

    return $router;
};
